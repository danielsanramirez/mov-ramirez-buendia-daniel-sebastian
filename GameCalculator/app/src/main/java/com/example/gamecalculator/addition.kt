package com.example.gamecalculator

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import kotlin.random.Random

class addition : Fragment() {

    internal lateinit var gameScoreTextView: TextView
    internal lateinit var gameRoundTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var num1TextView: TextView
    internal lateinit var num2TextView: TextView
    internal lateinit var goButton: Button
    internal lateinit var resultado: TextView

    @State
    var score = 0
    var round = 1

    var num1 = Random.nextInt(0,10)
    var num2 = Random.nextInt(0,10)

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    @State
    var timeLeft = 10

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_addition, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StateSaver.restoreInstanceState(this, savedInstanceState)

        gameRoundTextView = view.findViewById(R.id.round)
        gameScoreTextView = view.findViewById(R.id.score)
        num1TextView = view.findViewById(R.id.num1)
        num2TextView = view.findViewById(R.id.num2)
        timeLeftTextView = view.findViewById(R.id.time)
        goButton = view.findViewById(R.id.send)
        resultado = view.findViewById(R.id.result)

        gameRoundTextView.text = getString(R.string.round_d, round)
        gameScoreTextView.text = getString(R.string.score_d, score)
        num1TextView.text = getString(R.string.numero1,num1)
        num2TextView.text = getString(R.string.numero2,num2)

        resetGame()
        restoreGame()
    }

    private fun resetGame(){
        score = 0
        gameScoreTextView.text = getString(R.string.score_d, score)

        timeLeft = 10
        timeLeftTextView.text = getString(R.string.time_left_d, timeLeft)

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_d, timeLeft)
            }
        }
    }

    private fun verificarResultado(){
        var resu = num1 + num2
        //restoreGame()
        if(resultado.text.toString() == resu.toString()){
            countDownTimer.cancel()
            if(timeLeft <10 && timeLeft >=8){
                score = 100
                endGame()
            }
            if(timeLeft<8 && timeLeft >=5){
                score = 50
                endGame()
                //restoreGame()
            }
            if(timeLeft<5 && timeLeft >=0){
                score = 10
                endGame()
                //restoreGame()
            }
        }else{
            round ++
            if(round <= 5){
                gameRoundTextView.text = getString(R.string.round_d, round)
            }
        }
    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score_d, score)

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_d, timeLeft)
            }
        }
        countDownTimer.start()
        goButton.setOnClickListener { verificarResultado() }


    }

    private fun endGame(){
        Toast.makeText(activity, getString(R.string.end_game, score), Toast.LENGTH_LONG).show()
        gameScoreTextView.text = getString(R.string.score_d, score)
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

    }
}
