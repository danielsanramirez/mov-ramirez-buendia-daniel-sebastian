package com.example.time_fighter

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_welcome.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MainGame.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MainGame.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainGame : Fragment() {

    internal lateinit var welcomeMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button

    val db = FirebaseFirestore.getInstance()
    var name = ""

    @State
    var score = 0
    @State
    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    @State
    var timeLeft = 10

    private val args: MainGameArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StateSaver.restoreInstanceState(this, savedInstanceState)

        gameScoreTextView = view.findViewById(R.id.score)
        goButton = view.findViewById(R.id.go_button)
        timeLeftTextView = view.findViewById(R.id.time_left)

        //if(savedInstanceState != null){
        //    score = savedInstanceState.getInt(SCORE_KEY)
        //}

        gameScoreTextView.text = getString(R.string.score, score)

        welcomeMessage = view.findViewById(R.id.welcome_message)
        val playerName = args.playerName
        name = playerName
        welcomeMessage.text = getString(
            R.string.welcome_player,
            playerName.toString())

        goButton.setOnClickListener { incrementScore() }
//        gameScoreTextView.text = getString(R.string.score, score)

        if(gameStarted){
            restoreGame()
            return
        }
        resetGame()

    }

    private fun incrementScore() {
        if(!gameStarted){
            startGame()
        }

        score ++

        gameScoreTextView.text = getString(R.string.score, score)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this,outState)
        countDownTimer.cancel()
        //outState.putInt(SCORE_KEY,score)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    private fun resetGame(){
        score = 0
        gameScoreTextView.text = getString(R.string.score, score)

        timeLeft = 10
        timeLeftTextView.text = getString(R.string.time_left, timeLeft)

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft)
            }
        }
        gameStarted = false
    }

    private fun startGame(){
            countDownTimer.start()
            gameStarted = true

    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score, score)

        countDownTimer = object : CountDownTimer(timeLeft * 1000L, countDownInterval) {
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft)
            }
        }
        countDownTimer.start()
    }

    private fun endGame(){
        Toast.makeText(activity, getString(R.string.end_game, score), Toast.LENGTH_LONG).show()

        val data = HashMap<String, Any>()
        data["name"] = name
        data["score"] = score

        db.collection("players")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.d("MAIN_GAME", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("MAIN_GAME", "Error adding document", e)
            }
        resetGame()
    }

    companion object {
        private val SCORE_KEY = "SCORE"
    }
}
