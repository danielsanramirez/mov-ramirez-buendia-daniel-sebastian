class Singleton(
    var timeZone: String = "UTC/GMT -5"
){
    companion object {
        private var instance: Singleton ? = null
        //metodo estatico
        fun getInstance(): Singleton{
            if(instance == null){
                instance = Singleton()
                print("Object Created\n")
            }
            return instance!!
        }
    }

}


fun client(){
    val singleton = Singleton.getInstance()
    print("Time Zone: ${singleton.timeZone}\n")

    val singleton2 = Singleton.getInstance()
    print("Time Zone: ${singleton.timeZone}\n")

    if(singleton === singleton2){
        print("Singleton is correct")
    }else{
        print("Singleton is not correct")
    }

}

fun main(){
    client()
}
