import java.util.*

class libreria(val name: String)

class Daniel : Observable() {

    val name = "Daniel"

    fun prestarLibro(name : String){
        var libro = libreria(name)

        setChanged()
        notifyObservers(libro)
    }
}

class Gabriela : Observer{

    val name = "Gabriela"

    override fun update(o: Observable?, arg: Any?) {
        when (o){
            is   Daniel -> {
                if (arg is libreria){
                    println("$name esta teniendo ${arg.name} prestado por ${o.name}")
                }
            }
            else -> println(o?.javaClass.toString())
        }
    }
}

fun main(args : Array<String>){
    val bob = Daniel()
    //Provide Bob and instance of Tina
    bob.addObserver(Gabriela())
    bob.prestarLibro("Matemáticas del Prepo")
}
