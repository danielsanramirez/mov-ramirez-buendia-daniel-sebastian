interface ICurrency {
    fun symbol(): String
    fun code(): String
}

class Euro : ICurrency {
    override fun symbol(): String {
        return "€"
    }
    override fun code(): String {
        return "EUR"
    }
}

class UnitedStatesDollar : ICurrency {
    override fun symbol(): String {
        return "$"
    }
    override fun code(): String {
        return "USD"
    }
}

enum class Country {
    UnitedStates, Spain, UK, Greece
}

fun currency(country: Country): ICurrency? {
    when (country) {
        Country.Spain, Country.Greece -> return Euro()
        Country.UnitedStates -> return UnitedStatesDollar()
        else -> return null
    }
}

fun main(args: Array<String>) {
    val noCurrencyCode = "\"I am not Creative, so Currency Code Available\""

    println(currency(Country.Greece)?.symbol() ?: noCurrencyCode)
    println(currency(Country.Spain)?.code() ?: noCurrencyCode)
    println(currency(Country.UnitedStates)?.code() ?: noCurrencyCode)
    println(currency(Country.UK)?.code() ?: noCurrencyCode)
}