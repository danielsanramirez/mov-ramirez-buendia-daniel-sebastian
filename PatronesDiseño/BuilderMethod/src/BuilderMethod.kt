class BankAccount(
    var accountNumber: String?,
    var owner: String?,
    var branch: String?,
    var balance: Double?,
    var interestRate: Double?) {

    data class Builder(
        var accountNumber: String? = null,
        var owner: String? = null,
        var branch: String? = null,
        var balance: Double? = null,
        var interestRate: Double? = null){

        fun accountNumber(accountNumber: String) = apply { this.accountNumber = accountNumber }
        fun owner(owner: String) = apply { this.owner = owner }
        fun branch(branch: String) = apply { this.branch = branch }
        fun balance(balance: Double) = apply { this.balance = balance }
        fun interestRate(interestRate: Double) = apply { this.interestRate = interestRate }
        fun build() = BankAccount(accountNumber,owner,branch,balance,interestRate)
    }
}

val account = BankAccount.Builder()
    .accountNumber("1234L")
    .owner("Marge")
    .branch("Springfield")
    .balance(100.0)
    .interestRate(2.5)
    .build()

val anotherAccount = BankAccount.Builder()
    .accountNumber("4567L")
    .owner("Homer")
    .branch("Springfield")
    .balance(100.0)
    .interestRate(2.5)
    .build()

